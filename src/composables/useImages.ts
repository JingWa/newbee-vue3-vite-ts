import { reactive } from "vue"
import ImagesUrl from "../constant/imagesUrl"

export default function useImages (): any {
  const Images = reactive({
    logo: ImagesUrl.LogoPng
  })

  return {
    Images
  }
}
