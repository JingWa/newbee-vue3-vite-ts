export type HttpResponseType = {
  resultCode: Number,
  message: String,
  data: Object | String | null
}

export type UserInfoType = {
  userName: string,
  password: string
}

export type StateType = {
  token: String,
  userInfo: UserInfoType
}

export type SwiperItemType = {
  redirectUrl: string,
  carouselUrl: string
}

export type GridItemType = {
  name: string,
  imgUrl: string
}
