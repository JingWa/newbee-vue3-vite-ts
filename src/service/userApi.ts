import HTTP_SERVER from "./httpServer"
import { loginParamsType } from "../types/user"
import { HttpResponseType } from '@/types/common'
import ApiUrls from './apiUrls'

export const login = (params: loginParamsType): HttpResponseType => {
  return HTTP_SERVER.post(ApiUrls.Login, params)
}

export const register = (params: loginParamsType): HttpResponseType => {
  return HTTP_SERVER.post(ApiUrls.Register, params)
}
