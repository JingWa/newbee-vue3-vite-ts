import { createRouter, createWebHashHistory, RouteRecordRaw } from 'vue-router'
import Home from '@/views/Home/index.vue'
import Cart from '@/views/Cart/index.vue'
import Category from '@/views/Category/index.vue'
import User from '@/views/User/index.vue'
import Login from '@/views/Login/index.vue'
import Index from '@/views/index.vue'
import ProductList from '@/views/ProductList/index.vue'

const routes: Array<RouteRecordRaw> = [{
  path: '/',
  component: Index,
  redirect: '/home',
  children: [{
      path: '/home',
      component: Home
    }, {
      path: '/category',
      component: Category
    }, {
      path: '/cart',
      component: Cart
    }, {
      path: '/user',
      component: User
    }]
  }, {
    path: '/login',
    name: 'login',
    component: Login
  }, {
    path: '/productList',
    name: 'productList',
    component: ProductList
  }]

const router = createRouter({
  history: createWebHashHistory(),
  routes
})

export default router