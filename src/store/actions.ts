import { login, register } from "../service/userApi"

export default {
  asyncLogin (context: any, {payload}: any): Promise<Function> {
    return new Promise((resolve) => {
      resolve(login(payload))
    })
  },
  asyncRegister (context: any, {payload}: any): Promise<Function> {
    return new Promise((resolve, reject) => {
      resolve(register(payload))
    })
  }
}
