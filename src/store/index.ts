import { createStore } from 'vuex'
import actions from './actions'
import mutations from './mutations'
import createPersistedState  from 'vuex-persistedstate'

const state = {
  token: '',
  userInfo: {
    userName: '',
    password: ''
  }
}

export default createStore({
  plugins: [createPersistedState()],
  state,
  mutations,
  actions
})
