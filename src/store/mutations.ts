import { StateType, UserInfoType } from '../types/common'

export default {
  updateToken (state: StateType, payload: any) {
    console.log('payload', payload)
    state.token = payload.token
  },
  updateUserInfo (state: StateType, payload: UserInfoType) {
    state.userInfo = payload
  }
}